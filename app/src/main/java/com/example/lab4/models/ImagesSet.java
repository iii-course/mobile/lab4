package com.example.lab4.models;

import android.graphics.Bitmap;

public class ImagesSet {
    public Bitmap originalImage;
    public Bitmap convertedImage;

    public ImagesSet(Bitmap originalImage, Bitmap convertedImage) {
        this.originalImage = originalImage;
        this.convertedImage = convertedImage;
    }
}
