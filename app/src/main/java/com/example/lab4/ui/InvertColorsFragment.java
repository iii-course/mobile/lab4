package com.example.lab4.ui;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.lab4.R;

public class InvertColorsFragment extends Fragment {
    private ImageView invertedImageView;

    public InvertColorsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_invert_colors, container, false);
        invertedImageView = rootView.findViewById(R.id.invert_colors_inverted_image_view);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        applyInvertingFilter();
    }

    private void applyInvertingFilter() {
        float[] filterSrc = { -1, 0, 0, 0, 255,
                                0, -1, 0, 0, 255,
                                0, 0, -1, 0, 255,
                                0, 0, 0, 1, 0 };
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(new ColorMatrix(filterSrc));
        invertedImageView.setColorFilter(filter);
    }
}