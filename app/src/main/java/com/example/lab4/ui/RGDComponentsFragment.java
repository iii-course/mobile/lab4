package com.example.lab4.ui;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.lab4.R;

public class RGDComponentsFragment extends Fragment {
    private ImageView redImageView, greenImageView, blueImageView;

    public RGDComponentsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_r_g_d_components, container, false);
        redImageView = rootView.findViewById(R.id.rgb_components_r_image);
        greenImageView = rootView.findViewById(R.id.rgb_components_g_image);
        blueImageView = rootView.findViewById(R.id.rgb_components_b_image);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        applyAllFilters();
    }

    private void applyAllFilters() {
        applyRedFilter();
        applyGreenFilter();
        applyBlueFilter();
    }

    private void applyRedFilter() {
        float[] filterSrc = {
            1, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 1, 0
        };
        redImageView.setColorFilter(getColorFilter(filterSrc));
    }

    private void applyBlueFilter() {
        float[] filterSrc = {
                0, 0, 0, 0, 0,
                0, 1, 0, 0, 0,
                0, 0, 0, 0, 0,
                0, 0, 0, 1, 0
        };
        greenImageView.setColorFilter(getColorFilter(filterSrc));
    }

    private void applyGreenFilter() {
        float[] filterSrc = {
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 0, 1, 0
        };
        blueImageView.setColorFilter(getColorFilter(filterSrc));
    }

    private ColorMatrixColorFilter getColorFilter(float[] filterSrc) {
        return new ColorMatrixColorFilter(new ColorMatrix(filterSrc));
    }
}