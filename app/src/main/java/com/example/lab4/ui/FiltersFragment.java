package com.example.lab4.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.example.lab4.R;
import com.example.lab4.utils.helpers.BitmapHelpers;
import com.example.lab4.utils.filters.Filter;
import com.example.lab4.utils.filters.FilterFactory;

public class FiltersFragment extends Fragment {
    private Spinner chooseFilterSpinner;
    private ImageView imageView;
    private ProgressBar progressBar;

    private int defaultImageResId = R.drawable.img_building;
    private int imageToFilterResId = R.drawable.img_to_filter;
    private int currentImageResId = defaultImageResId;

    private int imageWidth = 600, imageHeight = 400;

    public FiltersFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_filters, container, false);

        setUpSpinner(rootView);
        setUpImageView(rootView);
        setUpProgressBar(rootView);

        return rootView;
    }

    private void onFilterSelected(int selectedItemPosition) {
        String[] filters = getResources().getStringArray(R.array.filters);
        String chosenFilter = filters[selectedItemPosition];
        currentImageResId = defaultImageResId;

        switch (chosenFilter) {
            case "Blur":
                blur();
                break;
            case "Sharpen":
                sharpen();
                break;
            case "Erosion":
                erode();
                break;
            case "Dilation":
                dilate();
                break;
            case "Sobel filter":
                applySobelFilter();
                break;
            case "Remove noise":
                currentImageResId = imageToFilterResId;
                applyMedianFilter();
                break;
            default:
                reset();
        }
    }

    private void applyMedianFilter() {
        startFilterThread(FilterFactory.createMedianFilter());
    }

    private void applySobelFilter() {
        startFilterThread(FilterFactory.createSobelFilter());
    }

    private void erode() {
        startFilterThread(FilterFactory.createErosionFilter());
    }

    private void dilate() {
        startFilterThread(FilterFactory.createDilationFilter());
    }

    private void sharpen() {
        startFilterThread(FilterFactory.createSharpenFilter());
    }

    private void blur() {
        startFilterThread(FilterFactory.createBlurFilter());
    }

    private void reset() {
        imageView.setImageBitmap(getOriginalImage());
    }

    private void startFilterThread(Filter filter) {
        Runnable runner = new Runnable() {
            public void run() {
                progressBar.post(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                });

                imageView.post(new Runnable() {
                    public void run() {
                        imageView.setImageBitmap(getOriginalImage());
                    }
                });

                Bitmap resultImage = filter.getFilteredImage(getOriginalImage());

                imageView.post(new Runnable() {
                    public void run() {
                        imageView.setImageBitmap(resultImage);
                    }
                });

                progressBar.post(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        };
        Thread t = new Thread(runner, "Filter Thread");
        t.start();
    }

    private Bitmap getOriginalImage() {
        return BitmapHelpers.scaleImage(
                BitmapFactory.decodeResource(
                    getResources(), currentImageResId
                ),
                imageWidth,
                imageHeight
        );
    }

    private void setUpSpinner(View rootView) {
        chooseFilterSpinner = rootView.findViewById(R.id.choose_filter_spinner);
        setListenerToSpinner();
    }

    private void setUpImageView(View rootView) {
        imageView = rootView.findViewById(R.id.filters_image_view);
        imageView.setImageBitmap(getOriginalImage());
    }

    private void setUpProgressBar(View rootView) {
        progressBar = rootView.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void setListenerToSpinner() {
        chooseFilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View itemSelected,
                                       int selectedItemPosition, long selectedId) {
                onFilterSelected(selectedItemPosition);
            }

            public void onNothingSelected(AdapterView<?> parent) {}

        });
    }
}