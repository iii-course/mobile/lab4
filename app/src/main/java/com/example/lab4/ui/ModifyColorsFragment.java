package com.example.lab4.ui;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lab4.R;
import com.example.lab4.models.Colors;

public class ModifyColorsFragment extends Fragment {
    private Button applyButton, resetButton;
    private EditText constantInput;
    private ImageView resultImageView;
    private Spinner colorsSpinner;

    public ModifyColorsFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_modify_colors, container, false);

        applyButton = rootView.findViewById(R.id.modify_colors_apply_button);
        resetButton = rootView.findViewById(R.id.modify_colors_reset_button);
        constantInput = rootView.findViewById(R.id.modify_colors_edit_text);
        resultImageView = rootView.findViewById(R.id.modify_colors_result_image_view);
        colorsSpinner = rootView.findViewById(R.id.choose_color_spinner);

        applyButton.setOnClickListener(this::onApplyButtonClick);
        resetButton.setOnClickListener(this::onResetButtonClick);

        return rootView;
    }

    private void onResetButtonClick(View view) {
        resultImageView.clearColorFilter();
    }

    private void onApplyButtonClick(View view) {
        resultImageView.clearColorFilter();
        try {
            ColorMatrixColorFilter filter = getColorFilter();
            resultImageView.setColorFilter(filter);
        } catch (Exception e) {
            makeToast(e.getMessage());
        }
    }

    private ColorMatrixColorFilter getColorFilter() throws Exception {
        return new ColorMatrixColorFilter(getColorMatrix());
    }

    private ColorMatrix getColorMatrix() throws Exception {
        Float constant = getConstantFromInput();
        if (constant != null) {
            Colors color = getColorFromInput();

            float[] filterSrc = {
                    1, 0, 0, 0, 0,
                    0, 1, 0, 0, 0,
                    0, 0, 1, 0, 0,
                    0, 0, 0, 1, 0
            };

            int constantIndex = mapToIndexInMatrix(color);
            if (constantIndex > -1) {
                filterSrc[constantIndex] = constant;
            }

            return new ColorMatrix(filterSrc);
        }
        throw new Exception("No constant entered");
    }

    private int mapToIndexInMatrix(Colors color) {
        switch (color) {
            case R:
                return 3;
            case G:
                return 7;
            case B:
                return 11;
            default:
                return -1;       }
    }

    private Colors getColorFromInput() {
        switch (colorsSpinner.getSelectedItem().toString()) {
            case "R":
                return Colors.R;
            case "G":
                return Colors.G;
            case "B":
                return Colors.B;
            default:
                return null;
        }
    }

    private Float getConstantFromInput() {
        String stringInput = constantInput.getText().toString();
        if (stringInput.length() > 0) {
            float input = Float.parseFloat(stringInput);
            return input > 1 ? 1 : input;
        }
        return null;
    }

    private void makeToast(String message) {
        Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }
}