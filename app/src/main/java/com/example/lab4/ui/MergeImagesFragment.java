package com.example.lab4.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.lab4.R;
import com.example.lab4.utils.helpers.BitmapHelpers;


public class MergeImagesFragment extends Fragment {
    private ImageView imageView1, imageView2, resultView;
    private EditText constantInput;
    private Button applyButton, resetButton;
    private ProgressBar progressBar;
    private int image1Res = R.drawable.img_mountains, image2Res = R.drawable.img_stars;
    private Float prevConstant;

    public MergeImagesFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_merge_images, container, false);

        applyButton = rootView.findViewById(R.id.merge_images_apply_button);
        resetButton = rootView.findViewById(R.id.merge_images_reset_button);
        constantInput = rootView.findViewById(R.id.merge_images_input);
        resultView = rootView.findViewById(R.id.merge_images_result_image);

        progressBar = rootView.findViewById(R.id.merge_images_progress_bar);

        imageView1 = rootView.findViewById(R.id.merge_images_original_image1);
        imageView2 =  rootView.findViewById(R.id.merge_images_original_image2);

        setImages();

        applyButton.setOnClickListener(this::onApplyButtonClick);
        resetButton.setOnClickListener(this::onResetButtonClick);

        progressBar.setVisibility(View.INVISIBLE);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        setImages();
    }

    private void onResetButtonClick(View view) {
        resultView.setVisibility(View.INVISIBLE);
    }

    private void onApplyButtonClick(View view) {
        Float constant = getConstantFromInput();
        resultView.setVisibility(View.VISIBLE);
        if (constant != null && !constant.equals(prevConstant)) {
            prevConstant = constant;
            startMergeThread(constant);
        }
    }

    private void startMergeThread(float proportion) {
        Runnable runner = new Runnable() {
            public void run() {
                progressBar.post(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                });
                Bitmap result = getMergedImage(proportion);
                resultView.post(new Runnable() {
                    @Override
                    public void run() {
                        resultView.setImageBitmap(result);
                    }
                });
                progressBar.post(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        };
        Thread t = new Thread(runner, "Merge Images Thread");
        t.start();
    }

    private Bitmap getMergedImage(float proportion) {
        int width = 600;
        int height = 400;

        Bitmap image1 = BitmapHelpers.scaleImage(
                BitmapFactory.decodeResource(getResources(), image1Res), width, height
        );
        Bitmap image2 = BitmapHelpers.scaleImage(
                BitmapFactory.decodeResource(getResources(), image2Res),  width, height
        );

        Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        int pixel1, pixel2, resultPixel;

        for (int i = 0; i < width; i += 1) {
            for (int j = 0; j < height; j += 1) {
                pixel1 = image1.getPixel(i, j);
                pixel2 = image2.getPixel(i, j);

                resultPixel = getResultPixel(pixel1, pixel2, proportion);

                result.setPixel(i, j, resultPixel);
            }
        }

        return result;
    }

    private int getResultPixel(int pixel1, int pixel2, float proportion) {
        int alpha, red, green, blue;

        float coef1 = proportion;
        float coef2 = 1 - proportion;

        alpha = Math.round(Color.alpha(pixel1) * coef1 + Color.alpha(pixel2) * coef2);
        red = Math.round(Color.red(pixel1) * coef1 + Color.red(pixel2) * coef2);
        green = Math.round(Color.green(pixel1) * coef1 + Color.green(pixel2) * coef2);
        blue = Math.round(Color.blue(pixel1) * coef1 + Color.blue(pixel2) * coef2);

        return Color.argb(alpha, red, green, blue);
    }

    private Float getConstantFromInput() {
        String stringInput = constantInput.getText().toString();
        if (stringInput.length() > 0) {
            float input = Float.parseFloat(stringInput);
            return input > 1 ? 1 : input;
        }
        return null;
    }

    private void setImages() {
        imageView1.setImageResource(image1Res);
        imageView2.setImageResource(image2Res);
    }
}