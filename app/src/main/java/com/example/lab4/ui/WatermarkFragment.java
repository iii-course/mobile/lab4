package com.example.lab4.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.lab4.R;
import com.example.lab4.models.ImagesSet;
import com.example.lab4.utils.helpers.BitmapHelpers;
import com.example.lab4.utils.filters.WatermarkFilter;

public class WatermarkFragment extends Fragment {
    private ViewPager2 viewPager;
    private WizardAdapter adapter;
    private ProgressBar progressBar;
    private int imgResId = R.drawable.img_building;
    private int imgWatermarkResId = R.drawable.img_watermark;

    public WatermarkFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_watermark, container, false);
        viewPager = rootView.findViewById(R.id.view_pager);
        progressBar = rootView.findViewById(R.id.progress_bar);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter = new WizardAdapter(getOriginalImage(), getWatermark());
        viewPager.setAdapter(adapter);
    }

    private Bitmap getOriginalImage() {
        return BitmapFactory.decodeResource(getResources(), imgResId);
    }

    private Bitmap getWatermark() {
        return BitmapFactory.decodeResource(getResources(), imgWatermarkResId);
    }

    private class WizardAdapter extends RecyclerView.Adapter<WizardAdapter.ViewHolder>  {
        private ImagesSet[] images;
        private Bitmap originalImage, imageWithWatermark,
                originalImageBlueChannel, imageWithWatermarkBlueChannel,
                watermark, watermarkBlackAndWhite;
        private final int
                WATERMARK_PAGE = 0,
                IMAGES_PAGE = 1,
                IMAGES_BLUE_CHANNEL_PAGE = 2;

        public WizardAdapter(Bitmap originalImage, Bitmap watermark) {
            this.originalImage = originalImage;
            this.watermark = watermark;
        }

        @Override
        public WizardAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.watermark_images, viewGroup, false);

            return new WizardAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(WizardAdapter.ViewHolder viewHolder, final int position) {
            Runnable runner = new Runnable() {
                public void run() {
                    setInProgress(true);
                    switchToPage(viewHolder, position);
                    setInProgress(false);
                }
            };
            Thread t = new Thread(runner, "Watermark Thread");
            t.start();
        }

        @Override
        public int getItemCount() {
            return 3;
        }

        private void switchToPage(WizardAdapter.ViewHolder viewHolder, int position) {
            ImagesSet imagesToSet;
            switch (position) {
                case WATERMARK_PAGE:
                    setWatermark();
                    imagesToSet = new ImagesSet(watermark, watermarkBlackAndWhite);
                    break;
                case IMAGES_PAGE:
                    setImageWithWatermark();
                    imagesToSet = new ImagesSet(originalImage, imageWithWatermark);
                    break;
                case IMAGES_BLUE_CHANNEL_PAGE:
                    setLSBImages();
                    imagesToSet = new ImagesSet(
                            originalImageBlueChannel,
                            imageWithWatermarkBlueChannel
                    );
                    break;
                default:
                    imagesToSet = null;
            }
            setImages(viewHolder, imagesToSet);
        }

        private void setImages(WizardAdapter.ViewHolder viewHolder, ImagesSet imagesToSet) {
            if (imagesToSet != null) {
                ImageView imageViewWithoutWatermark = viewHolder.getImageViewWithoutWatermark();
                imageViewWithoutWatermark.post(new Runnable() {
                    @Override
                    public void run() {
                        imageViewWithoutWatermark.setImageBitmap(imagesToSet.originalImage);
                    }
                });
                ImageView imageViewWithWatermark = viewHolder.getImageViewWithWatermark();
                imageViewWithWatermark.post(new Runnable() {
                    @Override
                    public void run() {
                        imageViewWithWatermark.setImageBitmap(imagesToSet.convertedImage);
                    }
                });
            }
        }

        private void setInProgress(boolean inProgress) {
            progressBar.post(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(inProgress ? View.VISIBLE : View.INVISIBLE);
                }
            });

            viewPager.post(new Runnable() {
                @Override
                public void run() {
                    viewPager.setVisibility(inProgress ? View.INVISIBLE : View.VISIBLE);
                }
            });
        }

        private void setWatermark() {
            if (watermarkBlackAndWhite == null) {
                watermarkBlackAndWhite = BitmapHelpers.toBlackAndWhite(watermark);
            }
        }

        private void setImageWithWatermark() {
            if (imageWithWatermark == null) {
                imageWithWatermark = WatermarkFilter.getImageWithWatermark(originalImage, watermark);
            }
        }

        private void setLSBImages() {
            setImageWithWatermark();
            if (originalImageBlueChannel == null) {
                originalImageBlueChannel = BitmapHelpers.getLSBImage(originalImage);
            }
            if (imageWithWatermarkBlueChannel == null) {
                imageWithWatermarkBlueChannel = BitmapHelpers.getLSBImage(imageWithWatermark);
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView imageWithoutWatermark;
            private ImageView imageWithWatermark;

            public ViewHolder(View view) {
                super(view);
                imageWithoutWatermark = view.findViewById(R.id.image_without_watermark);
                imageWithWatermark = view.findViewById(R.id.image_with_watermark);
            }

            public ImageView getImageViewWithoutWatermark() {
                return imageWithoutWatermark;
            }

            public ImageView getImageViewWithWatermark() {
                return imageWithWatermark;
            }
        }
    }
}

