package com.example.lab4.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.lab4.R;
import com.example.lab4.models.ImagesSet;

public class ReadyWatermarkImagesFragment extends Fragment {
    private ViewPager2 viewPager;
    private WizardAdapter adapter;
    private int imgResId = R.drawable.img_building,
            imgWithWatermarkResId = R.drawable.img_building_with_watermark,
            imgLSBResId = R.drawable.img_building_lsb,
            imgWithWatermarkLSBResId = R.drawable.img_building_with_watermark_lsb,
            imgWatermarkResId = R.drawable.img_watermark,
            imgWatermarkBlackAndWhiteResId = R.drawable.img_watermark_black_n_white;

    public ReadyWatermarkImagesFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_watermark, container, false);
        viewPager = rootView.findViewById(R.id.view_pager);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        ImagesSet[] images = {
                new ImagesSet(
                        getBitmapFromResource(imgWatermarkResId),
                        getBitmapFromResource(imgWatermarkBlackAndWhiteResId)
                ),
                new ImagesSet(
                        getBitmapFromResource(imgResId),
                        getBitmapFromResource(imgWithWatermarkResId)
                ),
                new ImagesSet(
                        getBitmapFromResource(imgLSBResId),
                        getBitmapFromResource(imgWithWatermarkLSBResId)
                )

        };
        adapter = new WizardAdapter(images);
        viewPager.setAdapter(adapter);
    }

    private Bitmap getBitmapFromResource(int id) {
        return BitmapFactory.decodeResource(getResources(), id);
    }

    class WizardAdapter extends RecyclerView.Adapter<WizardAdapter.ViewHolder> {
        private ImagesSet[] images;

        public WizardAdapter(ImagesSet[] images) {
            this.images = images;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.watermark_images, viewGroup, false);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, final int position) {
            viewHolder.setImages(images[position]);
        }

        @Override
        public int getItemCount() {
            return images.length;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView imageWithoutWatermark;
            private ImageView imageWithWatermark;

            public ViewHolder(View view) {
                super(view);
                imageWithoutWatermark = view.findViewById(R.id.image_without_watermark);
                imageWithWatermark = view.findViewById(R.id.image_with_watermark);
            }

            public void setImages(ImagesSet images) {
                imageWithoutWatermark.setImageBitmap(images.originalImage);
                imageWithWatermark.setImageBitmap(images.convertedImage);
            }
        }
    }
}