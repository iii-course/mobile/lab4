package com.example.lab4.utils.filters;

public class FilterFactory {
    private static BasicKernelFilter basicKernelFilter = null;
    private static SobelFilter sobelFilter = null;
    private static MedianFilter medianFilter = null;

    public static Filter createBlurFilter() {
        createBasicKernelFilter();
        double[] kernel5 = {
                0.000789, 0.006581, 0.013347, 0.006581, 0.000789,
                0.006581, 0.054901, 0.111345, 0.054901, 0.006581,
                0.013347, 0.111345, 0.225821, 0.111345, 0.013347,
                0.006581, 0.054901, 0.111345, 0.054901, 0.006581,
                0.000789, 0.006581, 0.013347, 0.006581, 0.000789,
        };
        double[] kernel7 = {
                0.00000067, 0.00002292, 0.00019117, 0.00038771, 0.00019117, 0.00002292, 0.00000067,
                0.00002292, 0.00078633, 0.00655965 , 0.01330373, 0.00655965, 0.00078633, 0.00002292,
                0.00019117, 0.00655965, 0.05472157, 0.11098164, 0.05472157, 0.00655965, 0.00019117,
                0.00038771, 0.01330373 , 0.11098164, 0.22508352, 0.11098164 , 0.01330373, 0.00038771,
                0.00019117, 0.00655965, 0.05472157, 0.11098164, 0.05472157, 0.00655965, 0.00019117,
                0.00002292, 0.00078633 , 0.00655965, 0.01330373, 0.00655965, 0.00078633, 0.00002292,
                0.00000067, 0.00002292 , 0.00019117, 0.00038771, 0.00019117, 0.00002292, 0.00000067,
        };
        basicKernelFilter.setKernel(kernel7);
        return basicKernelFilter;
    }

    public static Filter createSharpenFilter() {
        createBasicKernelFilter();
        double[] kernel = {
                -1, -1, -1,
                -1, 9, -1,
                -1, -1, -1,
        };
        basicKernelFilter.setKernel(kernel);
        return basicKernelFilter;
    }

    public static Filter createErosionFilter() {
        createBasicKernelFilter();
        float value = (float) 1 / 13;
        double[] kernel = {
                0, 0, value, 0, 0,
                0, value, value, value, 0,
                value, value, value, value, value,
                0, value, value, value, 0,
                0, 0, value, 0, 0,
        };
        basicKernelFilter.setKernel(kernel);
        return basicKernelFilter;
    }

    public static Filter createDilationFilter() {
        createBasicKernelFilter();
        double value = (double) 1 / 13;
        double[] kernel = {
                value, value, 0, value, value,
                value, 0, 0, 0, value,
                0, 0, 0, 0, 0,
                value, 0, 0, 0, value,
                value, value, 0, value, value,
        };
        basicKernelFilter.setKernel(kernel);
        return basicKernelFilter;
    }

    public static Filter createSobelFilter() {
        if (sobelFilter == null) {
            sobelFilter = new SobelFilter();
        }
        return sobelFilter;
    }

    public static Filter createMedianFilter() {
        if (medianFilter == null) {
            medianFilter = new MedianFilter();
        }
        return medianFilter;
    }

    private static void createBasicKernelFilter() {
        if (basicKernelFilter == null) {
            basicKernelFilter = new BasicKernelFilter();
        }
    }
}
