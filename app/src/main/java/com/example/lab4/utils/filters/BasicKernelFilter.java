package com.example.lab4.utils.filters;

import android.graphics.Bitmap;

import com.example.lab4.utils.helpers.MatrixHelpers;

public class BasicKernelFilter extends Filter {
    private double[] kernel;

    public BasicKernelFilter() {}

    public void setKernel(double[] kernel) {
        this.kernel = kernel;
    }

    @Override
    protected int getResultPixel(int row, int col, Bitmap image) {
        return applyKernelToPixel(row, col, image, kernel).getPixel();
    }

    @Override
    protected int getKernelSize() {
        if (kernel != null) {
            return MatrixHelpers.getMatrixSize(kernel);
        }
        return 0;
    }
}
