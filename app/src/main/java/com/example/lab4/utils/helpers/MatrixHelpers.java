package com.example.lab4.utils.helpers;

public class MatrixHelpers {
    public static int getMiddleIndex(int matrixSize) {
        return (int) Math.floor(matrixSize / 2);
    }

    public static int getIndexInOneDimensionalMatrix(int i, int j, int rowWidth) {
        return i * rowWidth + j;
    }

    public static int getMatrixSize(double[] matrix) {
        return (int) Math.sqrt(matrix.length);
    }
}
