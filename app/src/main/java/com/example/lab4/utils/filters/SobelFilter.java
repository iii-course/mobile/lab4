package com.example.lab4.utils.filters;

import android.graphics.Bitmap;

import com.example.lab4.utils.helpers.BitmapHelpers;
import com.example.lab4.utils.helpers.MatrixHelpers;

class SobelFilter extends Filter {
    private double[] xKernel = {
        1, 0, -1,
        2, 0, -2,
        1, 0, -1,
    };

    private double[] yKernel = {
        1, 2, 1,
        0, 0, 0,
        -1, -2, -1,
    };

    @Override
    protected int getResultPixel(int row, int col, Bitmap image) {
        RGBModel xDirection = applyKernelToPixel(row, col, image, xKernel);
        RGBModel yDirection = applyKernelToPixel(row, col, image, yKernel);

        int newR = calculateGradientMagnitude(xDirection.R, yDirection.R);
        int newG = calculateGradientMagnitude(xDirection.G, yDirection.G);
        int newB = calculateGradientMagnitude(xDirection.B, yDirection.B);

        return new RGBModel(newR, newG, newB).getPixel();
    }

    @Override
    protected int getKernelSize() {
        return MatrixHelpers.getMatrixSize(xKernel);
    }

    @Override
    protected Bitmap getImageReadyForFilter(Bitmap original, int requiredBorder) {
        Bitmap grayscale = BitmapHelpers.toGrayscale(original);
        return BitmapHelpers.getImageWithAddedBorder(grayscale, requiredBorder);
    }

    private int calculateGradientMagnitude(int x, int y) {
        return (int) Math.round(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
    }
}
