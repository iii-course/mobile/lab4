package com.example.lab4.utils.helpers;

public class StringsHelpers {
    public static String replaceCharAt(int index, String string, String newChar) {
        if (index <= 0) {
            return newChar + string.substring(1);
        } else if (index >= string.length() - 1) {
            return string.substring(0, string.length() - 1) + newChar;
        }
        return string.substring(0, index - 1) + newChar + string.substring(index);
    }
}
