package com.example.lab4.utils.helpers;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.util.Log;

import java.util.BitSet;

public class BitmapHelpers {
    public static Bitmap getImageWithAddedBorder(Bitmap original, int borderWidth) {
        int newHeight = original.getHeight() + borderWidth * 2;
        int newWidth = original.getWidth() + borderWidth * 2;
        return scaleImage(original, newWidth, newHeight);
    }

    public static int normalizeColorValue(int colorValue) {
        if (colorValue > 255) {
            return 255;
        }
        return Math.max(colorValue, 0);
    }

    public static Bitmap toGrayscale(Bitmap original) {
        // from https://gist.github.com/moondroid/c824ae418e83669583ff and
        // https://stackoverflow.com/questions/3373860/convert-a-bitmap-to-grayscale-in-android

        int height = original.getHeight();
        int width = original.getWidth();

        Bitmap grayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(grayscale);
        Paint paint = new Paint();

        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);

        c.drawBitmap(original, 0, 0, paint);

        return grayscale;
    }

    public static Bitmap toBlackAndWhite(Bitmap original) {
        Bitmap greyscale = toGrayscale(original);
        int width = greyscale.getWidth();
        int height = greyscale.getHeight();
        Bitmap newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < width; i += 1) {
            for (int j = 0; j < height; j += 1) {
                int pixel = original.getPixel(i, j);
                int newPixel = convertPixelToBlackOrWhite(pixel);
                newBitmap.setPixel(i, j, newPixel);
            }
        }
        return newBitmap;
    }

    public static Bitmap getLSBImage(Bitmap original) {
        int width = original.getWidth();
        int height = original.getHeight();
        Bitmap newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < width; i += 1) {
            for (int j = 0; j < height; j += 1) {
                int pixel = original.getPixel(i, j);
                int newPixel = getPixelFromLSBInBlueChannel(pixel);
                newBitmap.setPixel(i, j, newPixel);
            }
        }
        return newBitmap;
    }

    public static Bitmap scaleImage(Bitmap original, int width, int height) {
        return Bitmap.createScaledBitmap(original, width, height, false);
    }

    private static int convertPixelToBlackOrWhite(int pixel) {
        int alpha = 255;
        int middle = 128;
        int colors = (int) Math.round(0.299 * Color.red(pixel) +
                0.587 * Color.green(pixel) +
                0.114 * Color.blue(pixel));
        if (colors > middle) {
            colors = 255;
        } else {
            colors = 0;
        }
        return Color.argb(alpha, colors, colors, colors);
    }

    private static int getPixelFromLSBInBlueChannel(int pixel) {
        int alpha = 255;
        int lsb = getLSBInBlueChannel(pixel);
        int colors = lsb == 1 ? 255 : 0;
        return Color.argb(alpha, colors, colors, colors);
    }

    private static int getLSBInBlueChannel(int pixel) {
        String binary = Integer.toBinaryString(Color.blue(pixel));
        return Integer.valueOf(String.valueOf(binary.charAt(binary.length() - 1)));
    }
 }
