package com.example.lab4.utils.filters;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.example.lab4.utils.helpers.ArrayHelpers;
import com.example.lab4.utils.helpers.MatrixHelpers;

import java.util.ArrayList;
import java.util.Collections;

public class MedianFilter extends Filter {
    private int kernelSize = 5;

    protected int getResultPixel(int row, int col, Bitmap image) {
        return applyKernelToPixel(row, col, image, null).getPixel();
    }

    @Override
    protected RGBModel applyKernelToPixel(int row, int col, Bitmap image, double[] ignored) {
        int kernelSize = getKernelSize();

        ArrayList<Integer> R = new ArrayList<Integer>();
        ArrayList<Integer> G = new ArrayList<Integer>();
        ArrayList<Integer> B = new ArrayList<Integer>();

        int middle = MatrixHelpers.getMiddleIndex(kernelSize);

        for (int i = 0; i < kernelSize; i += 1) {
            for (int j = 0; j < kernelSize; j += 1) {
                int curRow = row + i - middle;
                int curCol = col + j - middle;
                int pixel = image.getPixel(curRow, curCol);
                R.add(Color.red(pixel));
                G.add(Color.green(pixel));
                B.add(Color.blue(pixel));
            }
        }

        Collections.sort(R);
        Collections.sort(G);
        Collections.sort(B);

        int middleIndex = ArrayHelpers.getMiddleIndex(R.size());

        return new RGBModel(
                R.get(middleIndex),
                G.get(middleIndex),
                B.get(middleIndex)
        );
    }

    @Override
    protected int getKernelSize() {
        return kernelSize;
    }
}