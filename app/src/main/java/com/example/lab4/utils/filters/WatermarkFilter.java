package com.example.lab4.utils.filters;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import com.example.lab4.utils.helpers.BitmapHelpers;
import com.example.lab4.utils.helpers.StringsHelpers;

public class WatermarkFilter {
    public static Bitmap getImageWithWatermark(Bitmap original, Bitmap watermark) {
        Bitmap watermarkBlackAndWhite = BitmapHelpers.toBlackAndWhite(watermark);

        int wmWidth = watermarkBlackAndWhite.getWidth();
        int wmHeight = watermarkBlackAndWhite.getHeight();
        int imgWidth = original.getWidth();
        int imgHeight = original.getHeight();

        Bitmap imageWithWatermark = Bitmap.createBitmap(imgWidth, imgHeight, Bitmap.Config.ARGB_8888);

        for (int i = 0; i < imgWidth; i++) {
            for (int j = 0; j < imgHeight; j ++) {
                int imgPixel = original.getPixel(i, j);
                int watermarkValue = getWatermarkValueAt(
                        i % wmWidth, j % wmHeight, watermarkBlackAndWhite
                );
                int newPixel = getPixelWithLSBChanged(imgPixel, watermarkValue);
                imageWithWatermark.setPixel(i, j, newPixel);
            }
        }

        return imageWithWatermark;
    }

    private static int getWatermarkValueAt(int i, int j, Bitmap watermark) {
        return Color.red(watermark.getPixel(i, j)) == 255 ? 1 : 0;
    }

    private static int getPixelWithLSBChanged(int pixel, int newLSBValue) {
        String blueByte = Integer.toBinaryString(Color.blue(pixel));
        String newBlueByte = StringsHelpers.replaceCharAt(
                blueByte.length() - 1, blueByte, String.valueOf(newLSBValue));

        int alpha = Color.alpha(pixel);
        int red = Color.red(pixel);
        int green = Color.green(pixel);
        int blue = Integer.parseInt(newBlueByte, 2);

        return Color.argb(alpha, red, green, blue);
    }
}
