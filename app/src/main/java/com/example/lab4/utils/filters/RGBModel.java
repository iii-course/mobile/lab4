package com.example.lab4.utils.filters;

import android.graphics.Color;

public class RGBModel {
    public int R;
    public int G;
    public int B;

    public RGBModel(int R, int G, int B) {
        this.R = R;
        this.G = G;
        this.B = B;
    }

    public int getPixel() {
        return Color.argb(255, R, G, B);
    }
}
