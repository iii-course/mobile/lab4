package com.example.lab4.utils.filters;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.example.lab4.utils.helpers.BitmapHelpers;
import com.example.lab4.utils.helpers.MatrixHelpers;

public class Filter {
    public Bitmap getFilteredImage(Bitmap original) {
        int matrixSize = getKernelSize();

        if (matrixSize > 0) {
            int requiredBorder = getRequiredBorder(matrixSize);

            int imageWidth = original.getWidth();
            int imageHeight = original.getHeight();

            Bitmap image = getImageReadyForFilter(original, requiredBorder);
            Bitmap resultImage = Bitmap.createBitmap(imageWidth, imageHeight, Bitmap.Config.ARGB_8888);

            for (int i = requiredBorder; i < imageWidth + requiredBorder; i += 1) {
                for (int j = requiredBorder; j < imageHeight + requiredBorder; j += 1) {
                    int resultPixel = getResultPixel(i, j, image);
                    resultImage.setPixel(i - requiredBorder, j - requiredBorder, resultPixel);
                }
            }

            return resultImage;
        }

        return original;
    }

    protected Bitmap getImageReadyForFilter(Bitmap original, int requiredBorder) {
        return BitmapHelpers.getImageWithAddedBorder(original, requiredBorder);
    }

    protected RGBModel applyKernelToPixel(int row, int col, Bitmap image, double[] kernel) {
        int kernelSize = getKernelSize();
        int middle = MatrixHelpers.getMiddleIndex(kernelSize);
        int sumRed = 0, sumGreen = 0, sumBlue = 0;

        for (int i = 0; i < kernelSize; i += 1) {
            for (int j = 0; j < kernelSize; j += 1) {
                int curRow = row + i - middle;
                int curCol = col + j - middle;
                int pixel = image.getPixel(curRow, curCol);

                int index = MatrixHelpers.getIndexInOneDimensionalMatrix(i, j, kernelSize);
                double multiplier = kernel[index];

                sumRed += (int) Math.round(Color.red(pixel) * multiplier);
                sumGreen += (int) Math.round(Color.green(pixel) * multiplier);
                sumBlue += (int) Math.round(Color.blue(pixel) * multiplier);
            }
        }

        return new RGBModel(
            BitmapHelpers.normalizeColorValue(sumRed),
            BitmapHelpers.normalizeColorValue(sumGreen),
            BitmapHelpers.normalizeColorValue(sumBlue)
        );
    }

    protected int getKernelSize() {
        return 0;
    }

    protected int getResultPixel(int row, int col, Bitmap image) {
        return 0;
    }

    private int getRequiredBorder(int matrixSize) {
        return (int) Math.floor((float) matrixSize / 2);
    }
}
