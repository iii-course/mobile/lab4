package com.example.lab4.utils.helpers;

public class ArrayHelpers {
    public static int getMiddleIndex(int arrayLength) {
        return (int) Math.floor((float) arrayLength / 2);
    }
}
